//
// <!-- Copyright (C) 2003-2010 Gurock Software GmbH. All rights reserved. -->
//

using Fabrica.Utilities.Drawing;

namespace Fabrica.Watch.SmartInspect
{
	internal class SessionInfo
	{
		public string Name;
		public Color Color;
		public bool HasColor;
		public Level Level;
		public bool HasLevel;
		public bool Active;
		public bool HasActive;
	}
}
