﻿namespace Fabrica.Mediator.Requests
{

    
    public interface IDeleteRequest
    {

        string Uid { get; set; }

    }


}
