﻿namespace Fabrica.Mediator.Requests
{
    public interface IRetrieveRequest
    {

        string Uid { get; set; }

    }
}
