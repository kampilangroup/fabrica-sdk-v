﻿namespace Fabrica.Work.Topics
{
 
    
    public class TopicEndpoint: ITopicEndpoint
    {

        public string Topic { get; set; } = "";

        public string Name { get; set; } = "";

        public string Path { get; set; } = "";


    }


}